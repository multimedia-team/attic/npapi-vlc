Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: npapi-vlc
Upstream-Contact: VLC media player developers <vlc-devel@videolan.org>
Source: http://git.videolan.org/?p=npapi-vlc.git
License: GPL-2+

Files: *
Copyright: 2002-2011 VideoLAN and VLC authors
License: GPL-2+

Files: activex/axvlc.idl
       activex/connectioncontainer.*
       activex/position.h
       activex/vlccontrol2.*
       npapi/control/npolibvlc.cpp
       npapi/control/position.h
Copyright: 2010 M2X BV
License: GPL-2+

Files: common/vlc_player_options.h
       common/win32_*
Copyright: 2002-2011 VideoLAN and VLC authors
License: LGPL-2.1+

Files: npapi/support/np*.cpp
Copyright: 2009 Jean-Paul Saman
License: LGPL-2.1+

Files: npapi/support/npunix.cpp
       npapi/support/npwin32.cpp
Copyright: 1998 Netscape Communications Corporation
License: LGPL-2.1+

Files: debian/*
Copyright: 2012 Benjamin Drung <bdrung@debian.org>
License: LGPL-2.1+

Files: debian/copyright
Copyright: 2012 Rafaël Carré <funman@videolan.org>
License: LGPL-2.1+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in the /usr/share/common-licenses/GPL-2 file.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2 can be found in the file `/usr/share/common-licenses/LGPL-2'.
